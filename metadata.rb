# frozen_string_literal: true

name             'gitlab-packagecloud'
maintainer       'GitLab Inc.'
maintainer_email 'ops-contact@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-packagecloud'
version          '0.2.36'

depends 'packagecloud', '1.0.1'
depends 'gitlab-vault'
