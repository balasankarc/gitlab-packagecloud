# Packagecloud GPG key rotation procedure

In this document we explain how to generate a new metadata signing key for
packages.gitlab.com. This procedure would be needed in case our GPG private key
ever gets compromised.

## Generate the new private key

This would be done on an administrator's laptop. You need GPG (`brew install
gpg` or `apt-get install gnupg`).

The goal is to generate a new private key on disk, add the key to Chef, write
down the key ID and DELETE THE KEY FROM DISK.

```
# Locally generate a new signing key
export gpg_tmp=/tmp/gpg-$(date +%s)
(umask 077 && mkdir "${gpg_tmp}")
gpg --homedir "${gpg_tmp}" --gen-key
```

- RSA (sign only)
- 4096 bits
- expiry: 5y (5 years)
- Real name: GitLab B.V.
- email: packages@gitlab.com
- comment: package repository signing key
- password: none (just press Enter)

```
# extract the secret key, as JSON
gpg --homedir "${gpg_tmp}" --export-secret-key --armor packages@gitlab.com | \
  ruby -rjson -e 'puts JSON.dump(ARGF.read)'
# extract the key ID. WRITE THIS DOWN
gpg --homedir "${gpg_tmp}" --list-keys packages@gitlab.com | \
  awk '/^pub/ { split($2, a, "/"); print "key ID:", a[2] }'
```

```
# in your chef-repo:
# Update the gpg_private_key field with the JSON string you created above.
# 
# Mention the key ID in the commit message!
#
bundle exec rake edit_role_secrets[packages-gitlab-com]
```

```
# delete your local copy of the new GPG key
rm -ri "${gpg_tmp}" # will ask for confirmation on each file
```

on packages.gitlab.com

```
sudo chef-client # will install the new private key
sudo packagecloud-ctl reindex-all
```

Check the key ID served by packages.gitlab.com. It should match the ID of the
key you generated above.

```
curl --silent https://packages.gitlab.com/gpg.key | \
  gpg --list-packets | \
  awk '/\tkeyid: / { print "key ID: ", substr($2, 9, 8) }'
```

## Revocation instructions for users

```
All users need to rotate the GPG key for packages.gitlab.com on their servers
immediately. Our new GPG package key ID is YYY-NEW-KEY-ID.

Debian/Ubuntu (APT):

    # Remove the old key
    sudo apt-key del XXX-OLD-KEY-ID
    
    # Manually inspect the list of keys
    apt-key list

    # add the new key
    wget -O - https://packages.gitlab.com/gpg.key | sudo apt-key add -

RHEL/Centos (YUM):

    # Remove the old key
    sudo find /var/lib/yum/repos -path '*/gitlab_gitlab*'

    # Download the new key
    sudo yum makecache

```
