# gitlab-packagecloud-cookbook

This cookbook manages Packagecloud Enterprise on packages.gitlab.com. See the
doc/ directory for more documentation.

## Upgrading packagecloud

Because packagecloud requires a manual 'reconfigure+restart' procedure when
upgrading, we use `apt-mark hold` to prevent `apt-get upgrade` from
automatically upgrading the packagecloud package. Running `apt-get install
packagecloud` will still upgrade the package when you want it to.

## Supported Platforms

TODO: List your supported platforms.

## Attributes

<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['gitlab-packagecloud']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

## Usage

### gitlab-packagecloud::default

Include `gitlab-packagecloud` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[gitlab-packagecloud::default]"
  ]
}
```

## Accessing Rails console

SSH into packagecloud and open the console with:

```
ssh packages.gitlab.com
sudo packagecloud-ctl console
```

## Download statistics

To get the downloads (CE,EE) of a certain period, open the Rails console and
run:

```
PublicDownload.where(created_at: PERIODSTART..PERIODEND).where(package_id: Package.where(repository_id: [8,11]).pluck(:id)).count
```