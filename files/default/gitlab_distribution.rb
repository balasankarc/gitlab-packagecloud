require_relative 'distribution'

module GitLabDistribution
  def find_valid_version!(osname, version_name, repo_file_ext, strict=true)
    if osname.downcase == "amazon" || osname.downcase == "aws" || osname.downcase == "amzn"
      osname = 'el'
      version_name = '7'
    end

    super(osname, version_name, repo_file_ext, strict)
  end
end

class Distribution
  class << self
    prepend GitLabDistribution
  end
end